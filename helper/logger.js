const winston = require('winston')
const DailyRotateFile = require('winston-daily-rotate-file')

module.exports = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp({ format: () => new Date().toLocaleString('en-IN', { timeZone: 'Asia/Kolkata' }) }),
        winston.format.simple(),
        winston.format.colorize(),
        // winston.format.prettyPrint()
    ),
    transports: [
        new DailyRotateFile({
            filename: `logs/%DATE%.log`,
            datePattern: 'DD-MM-YYYY',
            zippedArchive: true,
            maxFiles: '20d',
            prepend: true
        }),
        new winston.transports.Console
    ]
})