const { response } = require('../middlewares')
const { Comment } = require('../models')
const log = console.log
const objId = require('mongoose').Types.ObjectId

class Comments { }

Comments.Create = async (req, res) => {
    try {
        let { user: { userId } } = req
        body.userId = userId
        body.createdAt = new Date()
        body.updatedAt = new Date()
        let data = await Comment.create(req.body)
        response.create(req, res, data, 'Comments')
    } catch (e) {
        response.errors(req, res, 'Create Comments')
    }
}

Comments.GetAll = async (req, res) => {
    try {
        let { query: { start, limit, status, search, id } } = req
        let cond = { status: status || 'Y' }
        if (id) cond.blogId = new objId(id)
        if (search) cond.textMsg = RegExp(search, 'i')
        let data = await Comment.aggregate([
            { $match: cond },
            { $sort: { updatedAt: -1 } },
            { $skip: Number(start) || 0 },
            { $limit: Number(limit) || 10 },
            {
                $lookup: {
                    from: 'col__user',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'posted'
                }
            },
            { $unwind: { path: '$posted', preserveNullAndEmptyArrays: true } }
        ])
        response.GetAll(req, res, data, 'Comments')
    } catch (e) {
        log({ e })
        response.errors(req, res, 'GetAll Comments')
    }
}

Comments.Update = async (req, res) => {
    try {
        let { params: { id: _id }, body } = req
        let upt = await Comment.updateOne({ _id }, body)
        if (upt.modifiedCount) await Comment.updateOne({ _id }, { updatedAt: new Date() })
        response.update(req, res, upt, 'Comments')
    } catch (e) {
        response.errors(req, res, 'Update Comments')
    }
}

Comments.Status = async (req, res) => {
    try {
        let { body: { ids, status } } = req
        let upt = await Comment.updateOne({ _id: { $in: ids } }, { status })
        if (modifiedCount === ids?.length) await Comment.updateOne({ _id: { $in: ids } }, { updatedAt: new Date() })
        response.updateStatus(req, res, upt, 'Comments')
    } catch (e) {
        response.errors(req, res, 'Status Comments')
    }
}

Comments.Delete = async (req, res) => {
    try {
        let { body: { ids } } = req
        let del = await Comment.deleteMany({ _id: { $in: ids } })
        response.delete(req, res, del, 'Comments')
    } catch (e) {
        response.errors(req, res, 'Delete Comments')
    }
}

module.exports = Comments