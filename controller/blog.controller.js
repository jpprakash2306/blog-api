const { Blog } = require('../models')
const log = console.log
const objId = require('mongoose').Types.ObjectId

class Blogs { }

Blogs.Create = async (req, res) => {
    try {
        let { body: { title }, user: { userId: postedBy }, body } = req
        let found = await Blog.findOne({ title, postedBy })
        if (!found?.length) {
            let last = await Blog.find({ postedBy }).sort({ position: -1 })
            body.postedBy = postedBy
            body.position = (last?.[0]?.position + 1) || 1
            body.createdAt = new Date()
            body.updatedAt = new Date()
            let user = await Blog.create(req.body)
            if (user?._id)
                res.status(201).json({ msg: 'Blog Created Successfully', data: user })
            else
                res.status(417).json({ msg: 'Creating Blog Failure' })
        } else {
            res.status(208).json({ msg: `Already Existed Blog By You` })
        }
    } catch (e) {
        res.status(500).json({ msg: 'Error in Blog Creation' })
    }
}

Blogs.GetAllBlogs = async (req, res) => {
    try {
        let { query: { start, limit, status, search, id, userPosted } } = req
        let cond = { status: status || 'Y' }
        if (userPosted) cond.postedBy = await parseJwt(req, res)
        if (id) cond._id = new objId(id)
        if (search) cond.$or = [
            { title: RegExp(search, 'i') },
            { subTitle: RegExp(search, 'i') }
        ]
        let data = await Blog.aggregate([
            { $match: cond },
            { $sort: { position: 1 } },
            { $skip: Number(start) || 0 },
            { $limit: Number(limit) || 10 },
            {
                $lookup: {
                    from: 'col__user',
                    localField: 'postedBy',
                    foreignField: '_id',
                    as: 'posted'
                }
            },
            { $unwind: { path: '$posted', preserveNullAndEmptyArrays: true } }
        ])
        if (data?.length)
            res.status(200).json({ msg: 'Get Blogs Data Successfully', data })
        else
            res.status(208).json({ msg: 'No Blogs Found' })
    } catch (e) {
        log({ e })
        res.status(500).json({ msg: 'Error in Get Blogs Data' })
    }
}

Blogs.Update = async (req, res) => {
    try {
        let { params: { id: _id }, body } = req
        let { modifiedCount, matchedCount } = await Blog.updateOne({ _id }, body)
        if (modifiedCount) {
            await Blog.updateOne({ _id }, { updatedAt: new Date() })
            res.status(200).json({ msg: 'Blog Updated Successfully' })
        } else if (!modifiedCount && matchedCount) {
            res.status(208).json({ msg: 'Already Existed Value In Blog' })
        } else if (!matchedCount) {
            res.status(208).json({ msg: 'Blog Data Not Found' })
        } else
            res.status(208).json({ msg: 'Update Blog Failure' })
    } catch (e) {
        res.status(500).json({ msg: 'Error in Update Blog' })
    }
}

Blogs.Status = async (req, res) => {
    try {
        let { body: { ids, status } } = req
        let { modifiedCount, matchedCount } = await Blog.updateOne({ _id: { $in: ids } }, { status })
        if (modifiedCount === ids?.length) {
            await Blog.updateOne({ _id: { $in: ids } }, { updatedAt: new Date() })
            res.status(200).json({ msg: 'Blog Status Updated Successfully' })
        } else if (modifiedCount !== ids?.length && matchedCount === ids?.length) {
            res.status(208).json({ msg: 'Already Existed Value In Some Blogs' })
        } else if (matchedCount !== ids?.length) {
            res.status(204).json({ msg: 'Some Blogs Are Not Found' })
        } else
            res.status(307).json({ msg: 'Update Status Blog Failure' })
    } catch (e) {
        res.status(500).json({ msg: 'Error in Update Status Blog' })
    }
}

Blogs.Position = async (req, res) => {
    try {
        let { body } = req
        await body.forEach(async v => {
            let { modifiedCount } = await Blog.updateOne({ _id: v.id }, { position: v.position })
            if (modifiedCount) await Blog.updateOne({ _id: v.id }, { position: v.position })
        })
        res.status(200).json({ msg: 'Blogs Position Updated Successfully' })
    } catch (e) {
        res.status(500).json({ msg: 'Error in Update Position Blog' })
    }
}

Blogs.Delete = async (req, res) => {
    try {
        let { body: { ids } } = req
        let { deletedCount } = await Blog.deleteMany({ _id: { $in: ids } })
        if (deletedCount === ids?.length)
            res.status(200).json({ msg: 'Blogs Deleted Successfully' })
        else
            res.status(307).json({ msg: 'Deleting Blogs Failure' })
    } catch (e) {
        res.status(500).json({ msg: 'Error in Delete Blog' })
    }
}

async function parseJwt(req, res) {
    let token = req.headers.authorization
    if (!token) return res.status(400).json({ msg: 'Please Give Token' })
    if (!token?.length || !token.startsWith('Bearer ')) return res.status(401).json({ msg: 'Please Give Valid Token' })
    verify(token.slice(7), process.env.JWT_SECRETE_KEY, (err, decoded) => {
        if (!err) {
            return new objId(decoded.userId)
        } else {
            let { name, stack, expiredAt, message } = err
            if (name === 'TokenExpiredError' && message === 'jwt expired') {
                return res.status(201).json({ msg: 'Token Expired', data: expiredAt })
            }
            return res.status(500).json({ msg: 'Error in Token Verification', data: err })
        }
    })
}

module.exports = Blogs