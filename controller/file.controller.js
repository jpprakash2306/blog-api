const { v2: cloudinary } = require('cloudinary')
const { unlinkSync } = require('fs')
const log = console.log

class Files { }

Files.UploadProfile = async (req, res) => {
    try {
        await cloudinary.uploader.upload(req.file.path, { public_id: `profile-image-${req.user.userId}` }, (err, data) => {
            if (!err) {
                unlinkSync(req.file.path)
                res.status(200).json({ msg: 'Upload Profile Picture Successfully', data: data.secure_url })
            } else
                res.status(307).json({ msg: 'Upload Profile Failure' })
        })
    } catch (e) {
        res.status(500).json({ msg: 'Error in Upload Profile Picture' })
    }
}

Files.UploadBlogImage = async (req, res) => {
    try {
        await cloudinary.uploader.upload(req.file.path, { public_id: `blog-image-${req.user.userId}` }, (err, data) => {
            if (!err) {
                unlinkSync(req.file.path)
                res.status(200).json({ msg: 'Upload Profile Picture Successfully', data: data.secure_url })
            } else
                res.status(307).json({ msg: 'Upload Profile Failure' })
        })
    } catch (e) {
        res.status(500).json({ msg: 'Error in Upload Profile Picture' })
    }
}

Files.Delete = async (req, res) => {
    try {
        let id = req.body?.url?.split('/').pop().split('.')[0]
        let del = await cloudinary.uploader.destroy(id)
        if (del?.result === 'ok')
            res.status(200).json({ msg: 'Deleting Image Successfully' })
        else
            res.status(200).json({ msg: 'Deleting Image Failure', data: del.result })
    } catch (e) {
        res.status(500).json({ msg: 'Error in Delete In Cloud' })
    }
}

module.exports = Files