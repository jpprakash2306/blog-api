const { User, Contact, Chat } = require('../models')
const { sign } = require('jsonwebtoken')
const { genSaltSync, hashSync, compareSync } = require('bcrypt')
const { response } = require('../middlewares')
const log = console.log
const objId = require('mongoose').Types.ObjectId

class Users { }
Users.Login = async (req, res) => {
    try {
        let { userId, password } = req.body
        let found = await User.findOne({
            $or: [
                { mobile: userId },
                { email: userId }
            ]
        })
        if (!found?._id) return res.status(208).json({ msg: 'User Not Found' })
        if (compareSync(password, found?.password)) {
            let payload = {
                userId: found._id,
                userName: found.userName,
                userMobile: found.mobile
            }
            let token = sign(
                payload,
                process.env.JWT_SECRETE_KEY,
                { expiresIn: process.env.JWT_EXPIRED_TIME }
            )
            return response.success(req, res, token, 'Login successfully')
        } else
            response.warn(req, res, "Password is Incorrect")
    } catch (e) {
        response.errors(req, res, 'User Login')
    }
}

Users.Create = async (req, res) => {
    try {
        let { body: { mobile, email, password } } = req
        let found = await User.findOne({ $or: [{ mobile }, { email }] })
        if (!found?._id) {
            req.body.password = hashSync(password, genSaltSync(8))
            req.body.createdAt = new Date()
            req.body.updatedAt = new Date()
            let data = await User.create(req.body)
            response.create(req, res, data, 'User')
        } else {
            let credential = ''
            if (found.email === email && found.mobile === mobile) credential = 'Mobile Number & Email Id'
            if (found.mobile === mobile) credential = 'Mobile Number'
            if (found.email === email) credential = 'Email Id'
            res.status(208).json({ msg: `Already Existed ${credential}` })
        }
    } catch (e) {
        response.errors(req, res, 'User Registration')
    }
}

Users.Profile = async (req, res) => {
    try {
        let { userId } = req.user
        let data = await User.aggregate([
            { $match: { _id: new objId(userId) } },
            {
                $lookup: {
                    from: 'col__blog',
                    localField: '_id',
                    foreignField: 'postedBy',
                    pipeline: [{ $sort: { createdAt: -1 } }],
                    as: 'blogs'
                }
            }
        ])
        data = data[0]
        if (data?._id)
            response.success(req, res, data, 'Get User Profile Data Successfully')
        else
            response.success(req, res, data, 'User Profile Data Not Found')
    } catch (e) {
        response.errors(req, res, 'Get User Profile Data')
    }
}

Users.Update = async (req, res) => {
    try {
        let { user: { userId: _id }, body } = req
        let upt = await User.updateOne({ _id }, body)
        if (upt.modifiedCount) await User.updateOne({ _id }, { updatedAt: new Date() })
        response.update(req, res, upt, 'User')
    } catch (e) {
        response.errors(req, res, 'Update User Profile')
    }
}

module.exports = Users