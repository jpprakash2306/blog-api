
module.exports = {
    Users: require('./user.controller'),
    Blogs: require('./blog.controller'),
    Comment: require('./comment.controller'),
    File: require('./file.controller')
}