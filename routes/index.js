
module.exports = (app) => {
    app.use('/user', require('./user.routes'))
    app.use('/blog', require('./blog.routes'))
    app.use('/file', require('./file.routes'))
}