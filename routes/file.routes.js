const routes = require('express').Router()
const { File } = require('../controller')
const token = require('../middlewares/tokenverifiy')
const { extname } = require('path')
const multer = require('multer')

const profile = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => cb(null, '/tmp'),
        filename: (req, file, cb) => cb(null, `profile-${Date.now()}${file.originalname}`),
        fileFilter: (req, file, cb) => fileChecker(file, cb)
    }),
})

const blogImage = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => cb(null, '/tmp'),
        filename: (req, file, cb) => cb(null, `img-${Date.now()}${file.originalname}`, file),
        fileFilter: (req, file, cb) => fileChecker(file, cb)
    }),
})

function fileChecker(file, cb) {
    if (/jpeg|jpg|png|gif/.test(extname(file.originalname).toLowerCase()) && /jpeg|jpg|png|gif/.test(file.mimetype))
        return cb(null, true)
    else
        return cb(null, true)
}

routes.post('/profile', token, profile.single('profile'), File.UploadProfile)
routes.post('/blog', token, blogImage.single('image'), File.UploadProfile)
routes.delete('/', token, File.Delete)

module.exports = routes