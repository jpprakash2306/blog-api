const routes = require('express').Router()
const { Blogs } = require('../controller')
const token = require('../middlewares/tokenverifiy')
const { blogValid } = require('../validators')

routes.post('/', token, blogValid.create, Blogs.Create)
routes.get('/', blogValid.get, Blogs.GetAllBlogs)
routes.put('/update/:id', token, blogValid.update, Blogs.Update)
routes.put('/status', token, blogValid.status, Blogs.Status)
routes.put('/position', token, blogValid.position, Blogs.Position)
routes.delete('/', token, blogValid.delete, Blogs.Delete)

module.exports = routes