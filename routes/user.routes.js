const routes = require('express').Router()
const { Users } = require('../controller')
const token = require('../middlewares/tokenverifiy')
const { userValid } = require('../validators')

routes.post('/register', userValid.create, Users.Create)
routes.post('/login', userValid.login, Users.Login)
routes.get('/profile', token, Users.Profile)
routes.put('/profile', token, Users.Update)

module.exports = routes