const routes = require('express').Router()
const { Comment } = require('../controller')
const token = require('../middlewares/tokenverifiy')
const { blogValid } = require('../validators')

routes.post('/', token, blogValid.create, Comment.Create)
routes.get('/', token, Comment.GetAllComment)
routes.put('/update/:id', token, blogValid.update, Comment.Update)
routes.put('/status', token, blogValid.status, Comment.Status)
routes.put('/position', token, blogValid.position, Comment.Position)
routes.delete('/', token, blogValid.delete, Comment.Delete)

module.exports = routes