
const express = require('express')
const app = express()
const { createServer } = require('http')
const cors = require('cors')
const helmet = require('helmet')
const log = console.log
const { connect, connection } = require('mongoose')
const routes = require('./routes')
const cloudinary = require('cloudinary')

// Middlewares
app.use(cors())
app.use(helmet())
app.use(express.json())
routes(app)

// Loading Env
require('dotenv').config({})
let { APP_PORT, DB_NAME, DB_URL, CLOUDINARY_CLOUD_NAME, CLOUDINARY_API_KEY, CLOUDINARY_API_SECRET } = process.env

// Server Listening or Running
createServer(app).listen(APP_PORT, () => log('\nserver on  :', APP_PORT))

// DB Connection
connect(DB_URL + DB_NAME).catch(e => log('Error in DB Connect :', e))
connection.once('open', () => log('db connect :', DB_NAME, '\n'))

// Load Cloudinary
cloudinary.config({
    cloud_name: CLOUDINARY_CLOUD_NAME,
    api_key: CLOUDINARY_API_KEY,
    api_secret: CLOUDINARY_API_SECRET
})