const { model, Schema, Schema: { Types: { ObjectId } } } = require("mongoose")

module.exports = model('Blogs', new Schema({
    title: {
        type: String,
        required: true
    },
    postedBy: {
        type: ObjectId,
        required: true
    },
    subTitle: {
        type: String,
        required: true
    },
    content: {
        type: String,
        min: 1,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    position: {
        type: Number
    },
    status: {
        type: String,
        enum: ['Y', 'N', 'D'],
        default: 'Y'
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
        required: true
    }
},
    { collection: 'col__blog' }
))