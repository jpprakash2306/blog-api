const { model, Schema, Schema: { Types: { ObjectId } } } = require('mongoose')

module.exports = model('Chat', new Schema({
    blogId: {
        type: ObjectId,
        required: true
    },
    userId: {
        type: ObjectId,
        required: true
    },
    textMsg: {
        type: String,
        min: 1,
        required: true
    },
    status: {
        type: String,
        enum: ['Y', 'N', 'D'],
        default: 'Y'
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
        required: true
    }
}, { collection: 'col__comment' }
).index({ blogId: 1, userId: 1 }))