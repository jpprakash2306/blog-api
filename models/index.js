module.exports = {
    User: require('./user.model'),
    Blog: require('./blog.model'),
    Comment: require('./comment.model')
}