const Joi = require('joi')

const { valid } = require('../middlewares')

class UserValidation {
   create(req, res, next) {
      const schema = Joi.object({
         name: Joi.string().min(1).required(),
         mobile: Joi.number().min(1).required(),
         email: Joi.string().email().min(1).required(),
         password: Joi.string().min(3).required()
      })
      return valid.body(req, res, next, schema)
   }

   login(req, res, next) {
      const schema = Joi.object({
         userId: Joi.string().min(1).required(),
         password: Joi.string().min(1).required()
      })
      return valid.body(req, res, next, schema)
   }

   update(req, res, next) {
      const schema = Joi.object({
         name: Joi.string().min(1),
         mobile: Joi.number().min(1),
         email: Joi.string().email().min(1),
         password: Joi.string().min(3),
         profile: Joi.string().min(1)
      })
      return valid.body(req, res, next, schema)
   }
}

module.exports = new UserValidation()
