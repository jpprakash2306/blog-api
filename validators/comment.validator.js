const Joi = require('joi')
const { valid } = require("../middlewares")

class CommentValidation {

    create(req, res, next) {
        const schema = Joi.object({
            blogId: Joi.string().length(24).required(),
            textMsg: Joi.string().min(1).required()
        })
        return valid.body(req, res, next, schema)
    }

    get(req, res, next) {
        const schema = Joi.object({
            start: Joi.number().allow(''),
            limit: Joi.number().allow(''),
            status: Joi.string().allow('').valid('Y', 'N', 'D'),
            search: Joi.string().allow(''),
            id: Joi.string().length(24)
        })
        return valid.query(req, res, next, schema)
    }

    update(req, res, next) {
        const schema = Joi.object({
            title: Joi.string().min(1),
            subTitle: Joi.string().min(1),
            content: Joi.string().min(1),
            image: Joi.string().min(1)
        })
        return valid.body(req, res, next, schema)
    }

    status(req, res, next) {
        const schema = Joi.object({
            ids: Joi.array().items(Joi.string().length(24).required()).min(1).required(),
            status: Joi.string().valid('Y', 'N', 'D').required(),
        })
        return valid.body(req, res, next, schema)
    }

    position(req, res, next) {
        const schema = Joi.array().items(Joi.object({
            id: Joi.string().length(24).required(),
            position: Joi.number().min(1).required()
        }))
        return valid.body(req, res, next, schema)
    }

    delete(req, res, next) {
        const schema = Joi.object({
            ids: Joi.array().items(Joi.string().length(24).required()).min(1).required()
        })
        return valid.body(req, res, next, schema)
    }
}
module.exports = new CommentValidation()
