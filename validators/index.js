module.exports = {
    userValid: require("./user.validator"),
    blogValid: require('./blog.validator'),
    commentValid: require('./comment.validator')
};