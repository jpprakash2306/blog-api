module.exports = {
    response: require('./responses'),
    valid: require('./baseValidation'),
    verifyToken: require('./verifyToken')
}