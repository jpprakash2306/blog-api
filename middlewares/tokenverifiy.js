const { verify } = require('jsonwebtoken')

module.exports = (req, res, next) => {
    let token = req.headers.authorization
    if (!token) return res.status(400).json({ msg: 'Please Give Token' })
    if (!token?.length || !token.startsWith('Bearer ')) return res.status(401).json({ msg: 'Please Give Valid Token' })
    verify(token.slice(7), process.env.JWT_SECRETE_KEY, (err, decoded) => {
        if (!err) {
            req.user = decoded
            next()
        } else {
            let { name, stack, expiredAt, message } = err
            if (name === 'TokenExpiredError' && message === 'jwt expired') {
                return res.status(201).json({ msg: 'Token Expired', data: expiredAt })
            }
            return res.status(500).json({ msg: 'Error in Token Verification', data: err })
        }
    })
}