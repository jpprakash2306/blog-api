'use strict'

const { statusCodes, responseMsg, loggerMsg } = require('../constants')
const { logger } = require('../helper')
const { log } = console


class Response {
    // Create Response
    create(req, res, data, module) {

        if (data?._id) {
            logger.info(`${module} Created Successfully`)
            res.status(201).json({ msg: `${module} Created Successfully`, data })
        }
        else {
            logger.error(`Creating ${module} Failure`)
            res.status(417).json({ msg: `Creating ${module} Failure` })
        }
    }
    // GetAll Response
    getAll(req, res, data, module) {

        if (data?.length) {
            logger.info(`Get ${module}  Data Successfully`)
            res.status(200).json({ msg: ` Get ${module}  Data Successfully`, data })
        }
        else {
            logger.error(` No ${module} Found`)
            res.status(208).json({ msg: ` No ${module} Found` })
        }
    }
    // update Response
    update(req, res, data, module) {
        let { modifiedCount, matchedCount } = data

        if (modifiedCount) {
            logger.info(`${module} Updated Successfully `)
            res.status(200).json({ msg: `${module} Updated Successfully ` })
        }
        else if (!modifiedCount && matchedCount) {
            logger.info(`Already Existed Value In ${module}`)
            res.status(208).json({ msg: `Already Existed Value In ${module}` })
        }
        else if (!matchedCount) {
            logger.info(`${module} Data Not Found`)
            res.status(208).json({ msg: `${module} Data Not Found` })
        }
        else {
            logger.info(`Update ${module} Failure`)
            res.status(208).json({ msg: `Update ${module} Failure` })
        }
    }
    // updateStatus Response
    updateStatus(req, res, data, module) {
        let { modifiedCount, matchedCount } = data

        if (modifiedCount === ids?.length) {
            logger.info(`${module} Status Updated Successfully`)
            res.status(200).json({ msg: `${module} Status Updated Successfully` })
        }
        else if (modifiedCount !== ids?.length && matchedCount === ids?.length) {
            logger.info(`Already Existed Value In Some ${module}`)
            res.status(208).json({ msg: `Already Existed Value In Some ${module}` })
        }
        else if (matchedCount !== ids?.length) {
            logger.info(`Some ${module} Are Not Found`)
            res.status(204).json({ msg: `Some ${module} Are Not Found` })
        }
        else {
            logger.info(`Update Status ${module} Failure`)
            res.status(307).json({ msg: `Update Status ${module} Failure` })
        }
    }
    // Delete Response
    delete(req, res, data, module) {
        let { deletedCount } = data

        if (deletedCount === ids?.length) {
            logger.info(`${module} Deleted Successfully`)
            res.status(200).json({ msg: `${module} Deleted Successfully` })
        }
        else {
            logger.info(`Deleting ${module} Failure`)
            res.status(307).json({ msg: `Deleting ${module} Failure` })
        }
    }

    // Success Response
    success(req, res, data = null, msg = "Success") {
        logger.info(msg)
        return res.status(200).json({ status: 200, msg, data })
    }

    // Warning Response
    warn(req, res, module) {
        logger.warn(module)
        return res.status(208).json({
            status: 208,
            msg: `Warning In ${module}`
        })
    }

    // Failed Response
    failed(req, res, msg = req.baseUrl) {
        logger.warn(msg)
        return res.status(307).json({
            status: 307,
            msg: `Expectation Failed In ${msg}`
        })
    }

    // error Response
    errors(req, res, module) {
        logger.error(`Error In ${module}`)
        return res.status(500).json({
            status: 500,
            msg: `Error In ${module}`
        })
    }

    // joi error Response
    joierrors(req, res, err) {
        return res.status(400).json({
            status: 400,
            msg: "Bad Request",
            error: err.details.map(e => e.message.replace(/"/g, "")).join(', ')
        })
    }

}

module.exports = new Response()
