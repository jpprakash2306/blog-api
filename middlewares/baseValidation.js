'use strict'
const response = require("./responses")
const { statusCodes, responseMessage } = require('../constants')

class BaseValidation {

	body(req, res, next, schema) {
		try {
			const { error } = schema.validate(req.body)
			if (error) return response.joierrors(req, res, error)
			next()
		} catch (error) {
			console.log({ error })
			response.errors(req, res, statusCodes.HTTP_BAD_REQUEST, 'Bad Request')
		}
	};

	query(req, res, next, schema) {
		try {
			const { error } = schema.validate(req.query)
			if (error) return response.joierrors(req, res, error)
			next()
		} catch (error) {
			response.errors(req, res, statusCodes.HTTP_BAD_REQUEST, 'Bade Request')
		}
	};

}

module.exports = new BaseValidation()